﻿var canvas = new fabric.Canvas('c');
canvas.setWidth(301);
canvas.setHeight(301);
canvas.setBackgroundColor("#ffffff", () => {});
for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
        const gOptions = {
            width: 100,
            height: 100,
            top: 100 * j,
            left: 100 * i,
            hasControls: false,
            stroke: '#000000',
            fill: 'transparent',
            lockMovementX: true,
            lockMovementY: true,
            borderColor: 'transparent',
        } as fabric.IRectOptions;
        const rOptions = {
            width: 100,
            height: 100,
            top: 100 * j ,
            left: 100 * i,
            rx: 10,
            ry: 10,
            hasControls: false,
            stroke: '#000000',
            fill: 'transparent',
            lockMovementX: true,
            lockMovementY: true,
            borderColor: 'transparent',
            subType: 'button',
        } as any;
        var c = new fabric.Rect(gOptions);
        var r = new fabric.Rect(rOptions);
        r.setGradient('fill', {
            type: 'linear',
            x1: 0,
            y1: -r.height / 2,
            x2: 0,
            y2: r.height / 2,
            colorStops: {
                0: '#fff',
                1: '#eee'
            }
        } as fabric.IGradientOptions);
        canvas.add(c);
        canvas.add(r);
    }
}

canvas.on('mouse:over', (e) => {
    if (e.target) {
        if (e.target.get('subType') == 'button') {
            if (canvas.getActiveObject() == e.target) {
                return;
            }
            e.target.setGradient('fill', {
                type: 'linear',
                x1: 0,
                y1: -e.target.height / 2,
                x2: 0,
                y2: e.target.height / 2,
                colorStops: {
                    0: '#eee',
                    1: '#fff'
                }
            } as fabric.IGradientOptions);
            canvas.renderAll();
            e.target.hoverCursor = 'pointer';
        }
    }
});
canvas.on('mouse:out', (e) => {
    if (e.target) {
        if (e.target.get('subType') == 'button') {
            if (canvas.getActiveObject() == e.target) {
                return;
            }
            e.target.setGradient('fill', {
                type: 'linear',
                x1: 0,
                y1: -e.target.height / 2,
                x2: 0,
                y2: e.target.height / 2,
                colorStops: {
                    0: '#fff',
                    1: '#eee'
                }
            } as fabric.IGradientOptions);
            canvas.renderAll();
            e.target.hoverCursor = 'pointer';
        }
    }
});
canvas.on('object:selected', (e) => {
    canvas.getObjects().forEach((item: fabric.IObject) => {
        if (e.target.get('subType') == 'button') {
            item.setGradient('fill', {
                type: 'linear',
                x1: 0,
                y1: -item.height / 2,
                x2: 0,
                y2: item.height / 2,
                colorStops: {
                    0: '#fff',
                    1: '#eee'
                }
            } as fabric.IGradientOptions);
            canvas.renderAll();
        }

    });
    if (e.target.get('subType') == 'button') {
        console.log('button ' + (e.target as any).id + ' was clicked');
        e.target.setFill("#cc0000");
    }
});


